using System.Collections.Generic;

namespace Service
{
    public class UserCart
    {
        public string username;
        public List<Product> products;

        public UserCart(string username, List<Product> products)
        {
            this.username = username;
            this.products = products;
        }
    }
}