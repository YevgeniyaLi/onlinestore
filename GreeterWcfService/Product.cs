namespace Service
{
    public class Product
    {
        public string productName;
        public string description;
        public double price;

        public Product(string productName, string description, double price)
        {
            this.productName = productName;
            this.description = description;
            this.price = price;
        }
    }
}