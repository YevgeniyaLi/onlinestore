﻿using System.Collections.Generic;
using System.ServiceModel;

namespace Service
{
	[ServiceContract]
	public interface OnlineStore
	{
		[OperationContract]
		List<string> getProducts ();
		
		[OperationContract]
		string chooseProduct(string productName);

		[OperationContract]
		List<string> getProductsInCart(string username);
		
		[OperationContract]
		string addProductToCart (string username, string productName);

		[OperationContract]
		string buyProduct(string username, string productName, double money);
	}
}
