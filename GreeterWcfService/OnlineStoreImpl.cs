﻿using System.Collections.Generic;
using System.Linq;

namespace Service
{
    public class OnlineStoreImpl : OnlineStore
    {
        private List<UserCart> userCarts;
        private List<Product> products;

        public OnlineStoreImpl()
        {
            this.products = new List<Product>();
            products.Add(new Product("T-shirt", "White,casual style,XS size", 7.5));
            products.Add(new Product("Dress", "Red,long-sleeved,M size", 40.5));
            products.Add(new Product("Jeans", "Blue,skinny,S size", 50.0));
            products.Add(new Product("Jacket", "Black,rock style,S size", 56.7));
            
            this.userCarts = new List<UserCart>();
        }

        public string chooseProduct(string productName)
        {
            var product = products.Find(it => it.productName.Equals(productName));
            if (product != null)
            {
                return $"Product:{product.productName},\ndescription:{product.description},\nprice:{product.price}";
            }
            else
            {
                return $"No such product:{productName}";
            }
        }

        public List<string> getProducts()
        {
            return products.Select(it => it.productName).OrderBy(it => it).ToList();
        }

        public string addProductToCart(string username, string productName)
        {
            var product = products.Find(it => it.productName.Equals(productName));
            if (product != null)
            {
                var userCart = userCarts.Find(it => it.username.Equals(username));
                if (userCart == null)
                {
                    var products = new List<Product>();
                    products.Add(product);
                    userCart = new UserCart(username, products);
                    userCarts.Add(userCart);
                }
                else
                {
                    userCart.products.Add(product);
                }

                return $"Product:{productName} was successfully added to cart of user:{username}";
            }
            else
            {
                return $"No such product:{productName}";
            }
        }

        public List<string> getProductsInCart(string username)
        {
            return userCarts.Find(it => it.username.Equals(username))
                .products.Select(it => it.productName)
                .OrderBy(it => it).ToList();
        }

        public string buyProduct(string username, string productName, double money)
        {
            var userCart = userCarts.Find(it => it.username.Equals(username));
            if (userCart == null)
            {
                return "Nothing was added to user cart, please add product first to the cart";
            }

            var products = userCart.products;
            if (products == null || products.Count == 0)
            {
                return "Your cart is empty,plese add product first";
            }

            var productToBuy = products.Find(it => it.productName.Equals(productName));
            if (productToBuy == null)
            {
                return $"Product:{productToBuy} is not in your cart, please add it first";
            }

            var product = this.products.Find(it => it.productName.Equals(productName));
            var price = product.price;
            if (money >= price)
            {
                return $"Product:{productName} was successfully purchased,the rest:{money - price}";
            }
            else
            {
                return $"Not enough money to buy product:{productName},price is {price}";
            }
        }
    }
}