﻿// Client

using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("******** OnlineStore ********\n");
            Console.Write("\nEnter name: ");
            string username = Console.ReadLine();
            Console.WriteLine($"Wellcome {username}!!!\n");

            BasicHttpBinding binding = new BasicHttpBinding();
            EndpointAddress address = new EndpointAddress("http://localhost:8080");
            StoreClient storeClient = new StoreClient(username, binding, address);

            var options = new List<string>();
            options.Add("0. Get all products");
            options.Add("1. See product details");
            options.Add("2. Add product to cart");
            options.Add("3. See products in my cart");
            options.Add("4. Buy product");
            while (true)
            {
                Console.WriteLine("Choose one option:");
                foreach (var option in options)
                {
                    Console.WriteLine(option);
                }
                
                string input = Console.ReadLine();
                switch (input)
                {
                    case "0":
                    {
                        var result = storeClient.getProducts();
                        Console.WriteLine(result);
                    }
                        break;
                    case "1":
                    {
                        Console.WriteLine("Enter product name:");
                        string productName = Console.ReadLine();
                        var result = storeClient.chooseProduct(productName);
                        Console.WriteLine(result);
                    }
                        break;
                    case "2":
                    {
                        Console.WriteLine("Enter product name:");
                        string productName = Console.ReadLine();
                        var result = storeClient.addProductToCart(productName);
                        Console.WriteLine(result);
                    }
                        break;
                    case "3":
                    {
                        var result = storeClient.getMyProducts();
                        Console.WriteLine(result);
                    }
                        break;
                    case "4":
                    {
                        Console.WriteLine("Enter product name:");
                        string productName = Console.ReadLine();
                        Console.WriteLine("Enter money");
                        string moneyString = Console.ReadLine();
                        double money = Double.Parse(moneyString);
                        var result = storeClient.buyProduct(productName, money);
                        Console.WriteLine(result);
                    }
                        break;
                    default:
                    {
                        Console.WriteLine($"No such option:{input}");
                    }
                        break;
                }
            }
        }
    }
}