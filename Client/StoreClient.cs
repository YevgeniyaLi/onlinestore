﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Service;

namespace Client
{
	public class StoreClient : ClientBase<OnlineStore>
	{
		private string username;
		public StoreClient (string username, Binding binding, EndpointAddress address) : base (binding, address)
		{
			this.username = username;
		}

		public string chooseProduct (string productName)
		{
			return Channel.chooseProduct(productName);
		}
		
		public string getProducts ()
		{
			return String.Join("\n", Channel.getProducts());
		}
		
		
		public string getMyProducts ()
		{
			return String.Join("\n", Channel.getProductsInCart(username));
		}
		
		public string addProductToCart (string productName)
		{
			return Channel.addProductToCart(username, productName);
		}
		
		public string buyProduct (string productName, double money)
		{
			return Channel.buyProduct(username, productName, money);
		}
	}
}